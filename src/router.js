import express from "express";
import cats from "./cats.js";
const router = express.Router();

//To Do
router.get('/', (req, res) => res.send("This is the landing Page"));

//To Do
router.get('/cats', (req, res) => {
    return res.status(200).send(cats);
})

//ToDo
router.get('/cats/2', (req, res) => {
    return res.status(200).send(cats.filter(cat => cat.id == 2))
});


//To Do
// router.get('/cats/:id', (req, res) => {

//     return res.status (200).send (cats.find(cat => cat.id == req.params.id))
// });

router.get('/cats/:id', (req, res) => {
    let paramsId = req.params.id;
    let catIndex = cats.findIndex((catObj) => catObj.id == paramsId);
    return res.status(200).send(cats.findIndex(cat[catIndex]));
});

// To Do

router.post('/cats', (req, res) => {
    let newCat = {
        id: req.body.id,
        name: req.body.name,
        breed: req.body.breed
    };
    cats.push(newCat);
    return res.status(201).send(cats)
});

// ToDo 4

router.delete ("/cats", (req, res) => {
    const index = cats.findIndex((cat) => cat.id == req.body.id);
    cats.splice(index, 1);
    res.status(200).send(cats);

    })

    //to Do 6
    router.put("/cats/:id", (req, res) => {
        const index = cats.findIndex((cat) => cat.id == req.params.id);
        cats.splice(index, 1, req.body);
        res.status(200).send(cats);
    })



export default router;